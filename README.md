# Client Clinica
 
## Como usar?

1 - Clone o repositório com __git clone__;

2 - Se existir, remova o __package-lock.json__;

3 - Execute __npm install__;

4 - ✅ Execute __npm start__;