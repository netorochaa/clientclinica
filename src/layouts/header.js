import React from 'react';

const Header = () => (
    <header>
        <h2>Clínica José Neto</h2>
    </header>
);

export default Header;