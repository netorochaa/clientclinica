import React from 'react';
import Api from '../api/apiClinica';

class Patient extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cpf: '',
            name: ''
        };
    
        this.handleFocusOutCpf = this.handleFocusOutCpf.bind(this);
        this.handleChangeCPF = this.handleChangeCPF.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentFindPatientByCPF() {
        try{
            const response = await Api.get('patient/findByCPF/' + this.state.cpf);

            if(response.data.length > 0){
                response.data.map(p => (
                    this.setState({name: p.name})
                ));
                return;
            } 
        } catch(err) {
            console.log(err);
        }
        this.setState({name: ''})
    }

    handleFocusOutCpf() {
        this.setState({ name: "Procurando..." })
        this.componentFindPatientByCPF();
    }

    handleChangeCPF(event) {
        this.setState({cpf: event.target.value});
    }

    handleChangeName(event) {
        this.setState({name: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();

        const patient = {
            name: this.state.name,
            cpf: this.state.cpf
        };

        try {
            Api.post(`patient`, { patient })
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                })
        } catch(err) {
            console.log(err);
        }

        
    }
    render () {
        return (
            <div className="card">
                <h5>Cadastro de paciente</h5>
                <form onSubmit={this.handleSubmit}>
                    <label>
                    CPF:
                    <input type="text" value={this.state.cpf} onBlur={this.handleFocusOutCpf} onChange={this.handleChangeCPF}/>
                    </label>
                    <label>
                    Nome:
                    <input type="text" value={this.state.name} onChange={this.handleChangeName} />
                    </label>
                    <input type="submit" value="Enviar" />
                </form>
            </div>
        );
    }
    
};

export default Patient;