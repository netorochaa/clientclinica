
import React, { Component } from 'react';
import 'primereact/resources/themes/nova/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './App.css';
import Api from './api/apiClinica';
import {withRouter} from 'react-router-dom';

import Header from './layouts/header';
import Footer from './layouts/footer';
import {Menubar} from 'primereact/menubar';

class App extends Component {

  render () {
    const menu = [
      {
         label:'Home',
         icon:'pi pi-fw pi-home',
         command:() => this.props.history.push('/')
      },
      {
         label:'Iniciar atendimento',
         icon:'pi pi-fw pi-plus',
         command:() => this.props.history.push('/patient')
      },
   ];
    return(
      <div className="App">
          <Menubar model={menu}/>
          <Header />
          <Footer/>
          <div id="main">
            <main>
              <div className="content" id="content">
                {this.props.children}
              </div>
            </main>
          </div>
      </div>
    );
  }
}

export default withRouter(App);
