import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import Home from './components/home';
import Patient from './components/patient';
import Attendance from './components/attendance';
import Report from './components/report';

ReactDOM.render(
  (
    <Router>
      <App>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/patient" component={Patient}/>
          <Route path="/attendance" component={Attendance}/>
          <Route path="/Report" component={Report}/>
        </Switch>
      </App>
    </Router>
  ),
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
