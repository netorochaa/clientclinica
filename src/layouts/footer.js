import React from 'react';

const Footer = () => (
    <footer>
        <p>Clínica José Neto &copy; 2021</p>
    </footer>
);

export default Footer;